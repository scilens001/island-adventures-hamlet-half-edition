return {

    UI = {
        CUSTOMIZATIONSCREEN = {
            PRESETLEVELS = {
                SURVIVAL_PORKLAND_CLASSIC = "Hamlet",
            },
            PRESETLEVELDESC = {
                SURVIVAL_PORKLAND_CLASSIC = "An ancient pig kingdom, hidden deep within the jungle",
            },
            TASKSETNAMES = {
                PORKLAND = "Hamlet",
            },
        },
    },
}
