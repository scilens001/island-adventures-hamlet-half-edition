env._G = GLOBAL
_G.setmetatable(env,{__index=function(t,k) return _G.rawget(_G,k) end})

--asgerrr: i turned this off because the debug keys overlay on client mod controls (like TMIR console) in very annoying ways, and since almost every key has a debug binding there's really nowhere you can rebind the mod controls to
--debugcommands is pretty unintrusive tho
--_G.CHEATS_ENABLED = true
require("debugcommands")


modimport("scripts/main.lua")
