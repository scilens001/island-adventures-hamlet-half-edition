---@diagnostic disable lowercase-global

name = "Island Adventures - Hamlet"
description = "A currently very unfinished attempt to port the Hamlet DLC to DST in the same manner as Island Adventures - Shipwrecked."
author = "The Island Adventures Team"
version = "0.1"

local workshop_mod = folder_name and folder_name:find("workshop-") ~= nil

if not workshop_mod then
	name = "[Local] "..name
	description = description.."\n\n\n\nDeveloper version"
end

forumthread = ""
api_version = 10

dst_compatible = true

all_clients_require_mod = true
clients_only_mod = false

