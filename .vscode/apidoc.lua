---@meta

---@class Entity
Entity = {}

---@class Transform
Transform = {}

---@class Network
Network = {}

---@class Physics
Physics = {}

---@class AnimState
AnimState = {}

---@class SoundEmitter
SoundEmitter = {}

---@class Light
Light = {}

---@class DynamicShadow
DynamicShadow = {}

---@return Transform
---Adds a Transform component to an entity, setting (wrapping EntityScript).Transform to the created Transform object and returning it.
function Entity:AddTransform() end

---@return Network
---Adds a Network component to an entity, setting (wrapping EntityScript).Network to the created Network object and returning it.
function Entity:AddNetwork() end

---@return Physics
---Physicsdds a Physics component to an entity, setting (wrapping EntityScript).Physics to the created Physics object and returning it.
function Entity:AddPhysics() end

---@return AnimState
---AnimStatedds a AnimState component to an entity, setting (wrapping EntityScript).AnimState to the created AnimState object and returning it.
function Entity:AddAnimState() end

---@return SoundEmitter
---SoundEmitterdds a SoundEmitter component to an entity, setting (wrapping EntityScript).SoundEmitter to the created SoundEmitter object and returning it.
function Entity:AddSoundEmitter() end

---@return Light
---Lightdds a Light component to an entity, setting (wrapping EntityScript).Light to the created Light object and returning it.
function Entity:AddLight() end

---@return DynamicShadow
---DynamicShadowdds a DynamicShadow component to an entity, setting (wrapping EntityScript).DynamicShadow to the created DynamicShadow object and returning it.
function Entity:AddDynamicShadow() end



---@return boolean
---Returns whether an entity is currently in the rendered range of a player (this is a fair bit larger than the screen size, about 64 units radius)
function Entity:IsAwake() end


---@return string
---Gets some kind of internal "name" for an entity. Not the display name from the named component, not the prefab name. The name defaults to a single tab character (  ) if it has not been set. Appears to be unused and entirely pointless.
function Entity:GetName() end

---@param name string
---@return nil
---Sets some kind of internal "name" for an entity. Not the display name from the named component, not the prefab name. This "name" will be returned if GetName is called on the enity afterwards. Appears to be unused and entirely pointless.
function Entity:SetName(name) end

---@return integer
---Returns the GUID of the entity, which is the same as its index in the Ents global table.
function Entity:GetGUID() end

---@param tag string
---@return boolean
---Returns whether the entity has the specified tag.
function Entity:HasTag(tag) end


---@param number x
---@param number z
---@return nil
---AABB stands for Axis-aligned Bounding Box. This is called on some pillar-style prefab and looks like it's supposed to give them a rectangular hitbox, but does not appear to do anything.
function Entity:SetAABB(x, z) end

---@return boolean
---Returns whether the player is within a few tiles of the world origin. Entirely unused, and not exactly useful for anything beyond debugging rendering issues like it was probably intended for.
function Entity:FrustrumCheck() end

---@return nil
---Called as part of removing an entity, and seems to do just that, but without doing extra cleanup like calling OnRemoveEntity or removing child entities.
function Entity:Retire() end

---@return nil
---Makes the entity invisible. Obviously only matters if the entity has rendering from AddAnimState to begin with.
function Entity:Hide()

---@return nil
---Makes the entity visible again if it was previously hidden with Entity.Hide.
function Entity:Show()

---@return string
---Gets a string containing various debug information related to the various engine components on the entity.
function Entity:GetDebugString() end



---@class TheSim

TheSim = {}

---@param x number
---@param y number
---@param z number
---@param radius number
---@param must_have_all_tags string[]?
---@param cant_have_tags string[]?
---@param must_have_one_tags string[]?
---@return table
function TheSim:FindEntities(x, y, z, radius, must_have_all_tags, cant_have_tags, must_have_one_tags) end