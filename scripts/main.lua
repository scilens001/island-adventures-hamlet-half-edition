env._G = GLOBAL

local ToLoad = require("to_load")
PrefabFiles = ToLoad.Prefabs
Assets = ToLoad.Assets

for key, atlas in pairs(ToLoad.MiniMapAtlases) do
	print("DS - Atlas - Loading atlas ", atlas)
	-- This isn't working, it seems. All objects have lost their minimap icons again.
    AddMinimapAtlas(atlas)
end

for _, data in ipairs(ToLoad.InventoryItemsAtlasses) do
    for _, texture in ipairs(data.texture) do
        RegisterInventoryItemAtlas(data.atlas, texture)
    end
end

---- HAM Replicable Components ----
AddReplicableComponent("interiorplayer")

modimport("scripts/actions")
for k, v in pairs(require("ham_tunings")) do
    TUNING[k] = v
end

_G.UpvalueHacker =  require("tools/upvaluehacker")

modimport("scripts/standardcomponents")
modimport("scripts/constants")
modimport("scripts/patches")
modimport("scripts/recipes")
modimport("scripts/strings")
modimport("scripts/ham_fx")
modimport("scripts/ham_containers")
modimport("scripts/globalfunctions")

modimport("strings/names") -- So things have their display names


AddModCharacter("waterbot", "ROBOT")
table.insert(SEAMLESSSWAP_CHARACTERLIST, "waterbot") 
