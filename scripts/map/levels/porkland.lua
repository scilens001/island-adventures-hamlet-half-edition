local STRINGS = GLOBAL.STRINGS

AddLevel(LEVELTYPE.SURVIVAL, {
    id = "SURVIVAL_PORKLAND_CLASSIC",
    name = STRINGS.UI.CUSTOMIZATIONSCREEN.PRESETLEVELS.SURVIVAL_PORKLAND_CLASSIC,
    desc = STRINGS.UI.CUSTOMIZATIONSCREEN.PRESETLEVELDESC.SURVIVAL_PORKLAND_CLASSIC,
    location = "forest",
    version = 4,
    overrides = {
        roads = "never",
        task_set = "porkland",
        start_location = "PorklandStart",
        spring = "noseason",
        summer = "noseason",
        branching = "least",
        islands = "never",
    },

    background_node_range = {0, 1},

	numrandom_set_pieces = 0,
	random_set_pieces = {},
})