local STRINGS = GLOBAL.STRINGS

AddTaskSet("porkland", {
    name = STRINGS.UI.CUSTOMIZATIONSCREEN.TASKSETNAMES.PORKLAND,
    location = "forest",

    valid_start_tasks = {
	    "Edge_of_the_unknown"
	},


    numoptionaltasks = 0,
    optionaltasks = {},

    
    tasks = {
        "Pigtopia",
        "Pigtopia_capital",
        "Edge_of_civilization",
        "Edge_of_the_unknown",
        "Edge_of_the_unknown_2",
        "Lilypond_land",
        "Lilypond_land_2",
        "Deep_rainforest",
        "Deep_rainforest_2",
        "Deep_lost_ruins_gas",
        "Lost_Ruins_1",
        --"Lost_Ruins_4",
        "Deep_rainforest_3",
        "Deep_rainforest_mandrake",
        "Path_to_the_others",
        "Other_pigtopia_capital",
        "Other_pigtopia",
        "Other_edge_of_civilization",
        "this_is_how_you_get_ants",

        "Deep_lost_ruins4",
        "lost_rainforest",

        "Land_Divide_1",
        "Land_Divide_2",
        "Land_Divide_3",
        "Land_Divide_4",

        "painted_sands",
        "plains",
        "rainforests",
        "rainforest_ruins",
        "plains_ruins",
        "pinacle",

        "Deep_wild_ruins4",
        "wild_rainforest",
        "wild_ancient_ruins",
    },
    
    numrandom_set_pieces = 0,

    --asgerrr: these have to be commented out until implemented, as the world gen will just run, check for them, panic and loop over and over again otherwise
    --i mean that is what it is supposed to do if it fails to generate them
    required_prefabs = {
--[[        "pugalisk_fountain",
        "roc_nest",
        "pig_ruins_entrance",
        "pig_ruins_entrance2",
        "pig_ruins_entrance3",
        "pig_ruins_entrance4",
        "pig_ruins_entrance5",
        "pig_ruins_exit",
        "pig_ruins_exit2",
        "pig_ruins_exit4",
        "ancient_robot_ribs",
        "ancient_robot_head",
        ]]
    },
    required_prefab_count = {
        --[[
        ancient_robot_claw = 2,
        ancient_robot_leg  = 2,
        ]]
    },
})